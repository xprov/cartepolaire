#include <iostream>
#include <unistd.h>
// #include <X11/Xlib.h>   
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;
using namespace std;

// Parameters for HSV color ranges, pause and the choice of convertion
// cartesian to polar or polar to cartesian
typedef struct
{
  int iLowH, iHighH;  // hue range
  int iLowS, iHighS;  // saturation range
  int iLowV, iHighV;  // value range
  bool pause;         // pause display
  bool ctop;          // cartesian to polar
} ControlerData;


/**
 * Draws axes in the image
 *
 * There are no gradations on the axes since everything is qualitative.
 */
void addCentralAxis( Mat& M, const char* labelX, const char* labelY )
{
  int channels = M.channels();
  int nRows = M.rows;
  int nCols = M.cols * channels;
  int midRow = nRows / 2;
  int midCol = nCols / 2;
  for( int i = 0; i < nRows; ++i)
    {
      M.at<uchar>( i, midCol ) = 50;
    }
  for( int j = 0; j < nCols; ++j)
    {
      M.at<uchar>( midRow, j ) = 50;
    }
  putText( M, labelX, Point( nCols-30,nRows/2-2), FONT_HERSHEY_TRIPLEX, 1.0, Scalar( 0,0,0 ) );
  putText( M, labelY, Point( nCols/2+2,30), FONT_HERSHEY_TRIPLEX, 1.0, Scalar( 0,0,0 ) );
}


/**
 * Converts coordinates from cartesian to polar and the other way around.
 *
 * Static services : convert a whole image from cartesian to polar and the other
 * way around.
 */
class CoordsConverter
{
public:
  /**
   * Convert an image from cartesian coordinates to polar coordinates.
   *
   * Note : transformations on the pixels are computed the other way around in
   * order to avoid interpolation gaps between pixels.
   */
  static void cartesianToPolar(const Mat& org, Mat& cpy)
    {
      cpy = Scalar( 255, 255, 255 ); // white background

      // check that both matrices are compatible
      CV_Assert(org.depth() == cpy.depth() );
      CV_Assert(org.rows == cpy.rows );
      CV_Assert(org.cols == cpy.cols );

      int channels = org.channels();

      int nRows = org.rows;
      int nCols = org.cols * channels;

      CoordsConverter cc( nRows, nCols );

      for( int i = 0; i < nRows; ++i)
        {
          for ( int j = 0; j < nCols; ++j)
            {
              int I,J;
              cc.polarToCartesian( i, j/channels, I, J );
              J *= channels;
              if ( 0 <= I && I < nRows && 0 <= J && J < nCols )
                {
                  cpy.at<uchar>(i,j) = org.at<uchar>( I, J );
                }
            }
        }
    }

  /**
   * Convert an image from polar coordinates to cartesian coordinates.
   *
   * Note : transformations on the pixels are computed the other way around in
   * order to avoid interpolation gaps between pixels.
   */
  static void polarToCartesian(const Mat& org, Mat& cpy)
    {
      cpy = Scalar( 255, 255, 255 ); // white background

      // check that both matrices are compatible
      CV_Assert(org.depth() == cpy.depth() );
      CV_Assert(org.rows == cpy.rows );
      CV_Assert(org.cols == cpy.cols );

      int channels = org.channels();
      int nRows = org.rows;
      int nCols = org.cols * channels;
      CoordsConverter cc( nRows, nCols );
      for( int i = 0; i < nRows; ++i)
        {
          for ( int j = 0; j < nCols; ++j)
            {
              int I1,J1, I2,J2;
              cc.cartesianToPolar( i, j/channels, I1, J1, I2, J2 );
              J1 *= channels;
              J2 *= channels;
              bool b1 = ( 0 <= I1 && I1 < nRows && 0 <= J1 && J1 < nCols );
              bool b2 = ( 0 <= I2 && I2 < nRows && 0 <= J2 && J2 < nCols );
              if ( b1 && !b2 )
                cpy.at<uchar>(i,j) = org.at<uchar>( I1, J1 );
              else if ( b2 && !b1 )
                cpy.at<uchar>(i,j) = org.at<uchar>( I2, J2 );
              else if ( b1 && b2 )
                cpy.at<uchar>(i,j) = min( org.at<uchar>( I1,J1 ), org.at<uchar>( I2, J2 ) );
            }
        }
    }

private:

  CoordsConverter( int nrows, int ncols ) : nrows( nrows ), ncols( ncols )
  {
    fnrows = (double) nrows;
    fncols = (double) ncols;
    nmin = min( nrows, ncols );
    nmax = max( nrows, ncols );
    fnmin = min( nrows, ncols );
    fnmax = max( nrows, ncols );
  }

  /**
   * Polar (i,j) to cartesian (I,J).
   */
  void polarToCartesian( int i, int j, int& I, int& J )
    {
      i += ( ncols - nrows ) / 2;
      double theta = 2*M_PI * ( ncols/2-i ) / fncols;
      double r = (j-(ncols/2)) / fncols;
      double x = r * cos( theta );
      double y = r * sin( theta );
      I = ncols - (int) ( fncols * (y+.5) );
      J = (int) ( fncols * (x+.5) );
      I -= ( ncols - nrows ) / 2;
    }

  /**
   * Cartesian (i,j) to polar (I1,J1) and (I2, J2).
   *
   * There are two outputs because in polar coordinates (rho, theta) and
   * (-rho, pi+theta) are the same point.
   */
  void cartesianToPolar( int i, int j, int &I1, int &J1, int &I2, int& J2 )
    {
      i += ( ncols - nrows ) / 2;
      double x = (j-(ncols/2)) / fncols;
      double y = -( (i-ncols/2) / fncols );
      double r = sqrt( x*x + y*y );
      double theta = -atan2( y, x );
      I1 = (int) ( ( ( theta / (2*M_PI) ) + 0.5 ) * ncols );
      J1 = (int) ( (r+0.5)*fncols );

      theta += ( ( theta < 0.0 ) ? 1.0 : -1.0 ) * M_PI;
      I2 = (int) ( ( ( theta / (2*M_PI) ) + 0.5 ) * ncols );
      J2 = (int) ( (-r+0.5)*fncols );

      I1 -= ( ncols - nrows ) / 2;
      I2 -= ( ncols - nrows ) / 2;
    }


private :
    int nrows; 
    int ncols;
    int nmin;
    int nmax;
    double fnrows;
    double fncols;
    double fnmin;
    double fnmax;
};



void menu( const ControlerData& d )
{
  system("clear");
  cout << "Touches actives : \n\n";
  cout << "- c -\n";
  cout << "Inverse la transformation :\n";
  cout << "    cartésiennes --> polaires " << ( (d.ctop) ? " ***actuellement activée***" : "" ) << "\n";
  cout << "    polaires --> cartésiennes " << ( (!d.ctop) ? " ***actuellement activée***" : "" ) << "\n";
  cout << "\n";
  cout << "- espace -\n";
  cout << "Suspend/réactive la saisie de nouvelles images (pause).\n";
  if (d.pause)
    cout << "    *** actuellement en pause ***\n";
  cout << "\n";
  cout << "- r -\n";
  cout << "Règle le filtre de couleurs pour la détrection du ROUGE.\n";
  cout << "\n";
  cout << "- v -\n";
  cout << "Règle le filtre de couleurs pour la détrection du VERT.\n";
  cout << "\n";
  cout << "- b -\n";
  cout << "Règle le filtre de couleurs pour la détrection du BLEU.\n";
  cout << "\n";
  cout << "- n -\n";
  cout << "Règle le filtre de couleurs pour la détrection du NOIR.\n";
  cout << "\n";
  cout << "- ESC -\n";
  cout << "Quitte le programme\n";
  cout << endl;
}



int main( int argc, char** argv )
{
  //////////////////////////////
  //
  // Init capture device
  //
  VideoCapture cap;
  int deviceID = 0;             // 0 = open default camera
  int apiID = cv::CAP_ANY;      // 0 = autodetect default API
  cap.open(deviceID, apiID);
  // check if we succeeded
  //
  if (!cap.isOpened()) {
      cerr << "ERROR! Unable to open camera\n";
      return -1;
  }

  //
  //
  //////////////////////////////

  //////////////////////////////
  // 
  // Read arguments from commande line
  // 
  if ( argc >= 2 )
    {
      if ( strcmp( argv[1], "-petit" ) == 0 )
        {
          cap.set( CAP_PROP_FRAME_WIDTH, 320 );
          cap.set( CAP_PROP_FRAME_HEIGHT, 240 );
        }
      else if ( strcmp( argv[1], "-moyen" ) == 0 )
        {
          cap.set( CAP_PROP_FRAME_WIDTH, 640 );
          cap.set( CAP_PROP_FRAME_HEIGHT, 480 );
        }
      else if ( strcmp( argv[1], "-grand" ) == 0 )
        {
          cap.set( CAP_PROP_FRAME_WIDTH, 1280 );
          cap.set( CAP_PROP_FRAME_HEIGHT, 960 );
        }
      else 
        {
          cerr << "\n";
          cerr << "Usage : " << argv[0] << " [TAILLE]\n";
          cerr << "Valeurs possibles pour TAILLE : \n";
          cerr << "  -petit\n";
          cerr << "  Saisie des images de taille 320x240\n\n";
          cerr << "  -moyen\n";
          cerr << "  Saisie des images de taille 640x480\n\n";
          cerr << "  -grand\n";
          cerr << "  Saisie des images de taille 1280x960\n\n";
          exit( EXIT_FAILURE );
        }
    }
  else  // no args, set default
    {
      cap.set( CAP_PROP_FRAME_WIDTH, 640 );
      cap.set( CAP_PROP_FRAME_HEIGHT, 480 );
    }

  //
  //
  //////////////////////////////

  //////////////////////////////
  // 
  // Build control window
  // 

  ControlerData d = { 150, 210, 100, 255, 100, 255, false, true };

  namedWindow("Control", cv::WINDOW_NORMAL); //create a window called "Control"
  //Create trackbars in "Control" window
  createTrackbar("LowH", "Control", &d.iLowH, 255); //Hue (0 - 179)
  createTrackbar("HighH", "Control", &d.iHighH, 255);
  createTrackbar("LowS", "Control", &d.iLowS, 255); //Saturation (0 - 255)
  createTrackbar("HighS", "Control", &d.iHighS, 255);
  createTrackbar("LowV", "Control", &d.iLowV, 255); //Value (0 - 255)
  createTrackbar("HighV", "Control", &d.iHighV, 255);
  resizeWindow("Control", 500, 200);

  //
  //
  //////////////////////////////
  
 

  //////////////////////////////
  // 
  // Main loop
  //
  // 1. Get image from capture device
  // 2. Apply an horizontal flip so the it look like a miror
  // 3. Create a copy of the image and on this copy:
  //   3.1 apply gaussian blur to reduce noise,
  //   3.2 convert to HSV to enable color recogintion,
  //   3.3 do color recognition by applying a threshold according the values set
  //       by the user on the control window,
  //   3.4 apply morphological closing in order to erase noise on foreground,
  //   3.5 take the negative of the image for a nicer display (white becomes black
  //       and black becomes white)
  // 4. Convert from one coordinate system to the other (cartesian to polar or
  //     polar to cartesian)
  // 5. Draw axes systems centrer in the middle of both images (before and
  //     after the coordinate conversion)
  // 6. Show all three images : the original, the cartesian and the polar.
  // 7. Check keyboard for input and take action if so.
  // 

  Mat imgOriginal, imgTraitement;
  bool displayMenu = true;
  string lastActionDisplay("");

  bool quit = false;
  while (!quit)
    {
      // 1. Get image from capture device
      if ( ! d.pause )
        {
          cap.read(imgOriginal);
          if (imgOriginal.empty()) //if not success, break loop
            {
              cout << "Cannot read a frame from video stream" << endl;
              break;
            }
          // 2. Apply an horizontal flip so the it look like a miror
          flip(imgOriginal, imgOriginal, 1 );
        }

      // 3.1 apply gaussian blur to reduce noise,
      GaussianBlur( imgOriginal, imgTraitement, Size( 3, 3 ), 2 );
      // 3.2 convert to HSV to enable color recogintion,
      cvtColor(imgTraitement, imgTraitement, COLOR_BGR2HSV); // from BGR to HSV

      // 3.3 do color recognition by applying a threshold according the values set
      // by the user on the control window,
      if ( d.iHighH < 180 )
        {
          inRange(imgTraitement, 
                  Scalar( d.iLowH, d.iLowS, d.iLowV), 
                  Scalar( d.iHighH, d.iHighS, d.iHighV), 
                  imgTraitement); //Threshold the image
        }
      else
        {
          // Red spans at both sides of the range, the real range id 0--179.
          Mat imgRedHigh, imgRedLow;
          inRange(imgTraitement, 
                  Scalar( d.iLowH, d.iLowS, d.iLowV), 
                  Scalar( 255, d.iHighS, d.iHighV), 
                  imgRedHigh); //Threshold the image
          inRange(imgTraitement, 
                  Scalar( 0, d.iLowS, d.iLowV), 
                  Scalar( d.iHighH - 179, d.iHighS, d.iHighV), 
                  imgRedLow); //Threshold the image
          addWeighted( imgRedHigh, 1.0, imgRedLow, 1.0, 0.0, imgTraitement );
        }

      // 3.?  morphological opening to remove small objects from the foreground.
      //erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) );
      //dilate( imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) ); 

      // 3.4 apply morphological closing in order to erase noise on foreground,
      //morphological closing (fill small holes in the foreground)
      dilate( imgTraitement, imgTraitement, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) ); 
      erode( imgTraitement, imgTraitement, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) );

      // 3.5 take the negative of the image for a nicer display (white becomes black
      //     and black becomes white)
      bitwise_not( imgTraitement, imgTraitement );

      // 4. Convert from one coordinate system to the other (cartesian to polar or
      //    polar to cartesian)
      Mat copie( imgTraitement.rows, imgTraitement.cols, imgTraitement.type() );
      Mat * cartesian, *polar;
      if ( d.ctop )
        {
          cartesian = &imgTraitement;
          CoordsConverter::cartesianToPolar( imgTraitement, copie );
          polar = &copie;
        }
      else
        {
          polar = &imgTraitement;
          CoordsConverter::polarToCartesian( imgTraitement, copie );
          cartesian = &copie;
        }
      // 5. Draw axes systems centrer in the middle of both images (before and
      //    after the coordinate conversion)
      addCentralAxis( *cartesian, "x", "y" );
      addCentralAxis( *polar, "r", "theta" );
      // 6. Show all three images : the original, the cartesian and the polar.
      imshow("Original", imgOriginal); //show the original image
      imshow("Cartésienne", *cartesian); //show the thresholded image
      imshow("Polaire", *polar); //show the thresholded image

      // 7. Check keyboard for input and take action if so.
      if (displayMenu) {
          menu(d);
          cout << lastActionDisplay << std::endl;
          displayMenu = false;
      }

      bool changeValues = false;
      int k = waitKey(1);

      if ( k == 1048690 || k == 114 ) // r
        {
          lastActionDisplay = "# Détection de la couleur ``rouge``";
          d = { 150, 210, 100, 255, 100, 255, d.pause, d.ctop };
          changeValues = true;
        }
      else if ( k == 1048674 || k == 98 ) // b
        {
          lastActionDisplay = "# Détection de la couleur ``bleu``";
          d = { 90, 150, 100, 255, 100, 255, d.pause, d.ctop };
          changeValues = true;
        }
      else if ( k == 1048694 || k == 118 ) // v
        {
          lastActionDisplay = "\n# Détection de la couleur ``vert``";
          d = { 30, 90, 40, 255, 164, 255, d.pause, d.ctop };
          changeValues = true;
        }
      else if ( k == 1048686 || k == 110 ) // n
        {
          lastActionDisplay = "\n# Détection de la couleur ``noir``";
          d = { 0, 255, 0, 255, 0, 50, d.pause, d.ctop };
          changeValues = true;
        }
      else if ( k == 1048608 || k == 32 ) // space
        {
          d.pause = !d.pause;
          changeValues = true;
        }
      else if ( k == 1048675 || k == 99 ) // c
        {
          d.ctop = !d.ctop;
          changeValues = true;
        }
      else if (k == 1048603 || k == 27 ) // escape
        {
          cout << "Fin.\n" << endl;
          quit = true;
        }
      if ( changeValues )
        {
          setTrackbarPos( "LowH",  "Control", d.iLowH );
          setTrackbarPos( "HighH", "Control", d.iHighH );
          setTrackbarPos( "LowS",  "Control", d.iLowS );
          setTrackbarPos( "HighS", "Control", d.iHighS );
          setTrackbarPos( "LowV",  "Control", d.iLowV );
          setTrackbarPos( "HighV", "Control", d.iHighV );
          changeValues = false;
          displayMenu = true;
        }
    }

  //
  // main loop (end)
  //////////////////////////////

  return 0;
}
