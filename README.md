# Carte polaire #

Visualization tool for the conversion between polar and cartesian coordinates
using a webcam and color recognition.

* Polar square

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![demo1.gif](./demos/demo1.gif)

* Cartesian square

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![demo2.gif](./demos/demo2.gif)

* Polar segment

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![demo3.gif](./demos/demo3.gif)



## Setup ##

 -  Dependencies: `OpenCV`.

#### Using `cmake` (recommended)

 -  __Compilation__

	```
	mkdir build
	cd build
	cmake ..
	make
	```

 - __Run__

	From the `build` folder
	```
	./cartePolaire
	```

